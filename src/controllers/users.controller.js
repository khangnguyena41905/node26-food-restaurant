// Controller nhận vào request, respone
//  Nhiện vụ: chỉ parse request (param, body) và sau đó chuyển xuống Service xử lý, nhận kết quả trả về từ Service và trẻ Response ra cho client

const { response } = require("../helpers/respone");
const userService = require("../services/users.service");

const getUsers = () => {
  return async (req, res, next) => {
    try {
      const users = await userService.getUsers();
      res.status(200).json(response(users));
    } catch (error) {
      // res.status(500).json({ error: error.message });
      next(error);
    }
  };
};

const createUser = () => {
  return async (req, res, next) => {
    try {
      const user = req.body;
      const createdUser = await userService.createUser(user);
      res.status(200).json(response(createdUser));
    } catch (error) {
      // res.status(500).json({ error: error.message });
      next(error);
    }
  };
};

const deleteUser = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      const deletedUser = await userService.deleteUser(id);
      res.status(200).json("Delete user success");
    } catch (error) {
      // res.status(500).json({ error: error.message });
      next(error);
    }
  };
};

const updateUser = () => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      const data = req.body;
      const updatedUser = await userService.updateUser(id, data);
      // res.status(200).json({ data: updatedUser });
      res.status(200).json(response(updatedUser));
    } catch (error) {
      // res.status(500).json({ error: error.message });
      next(error);
    }
  };
};

module.exports = {
  getUsers,
  createUser,
  deleteUser,
  updateUser,
};
