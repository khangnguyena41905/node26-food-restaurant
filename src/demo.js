const express = require("express");
const { Sequelize, DataTypes } = require("sequelize");

const app = express();
app.use(express.json());

// Tạo kết nối db bằng Sequelize
const sequelize = new Sequelize("node26-food", "root", "1234", {
  host: "localhost",
  port: 3306,
  dialect: "mysql",
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Sequelize Connected");
  })
  .catch(() => {
    console.log("Sequelize Failed");
    throw error;
  });

// tạo model để sequelize liên kết tới table và lấy/thêm/sửa/xóa(crud) data
const User = sequelize.define(
  "User",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    firstName: {
      type: DataTypes.STRING(50),
      field: "first_name",
    },
    lastName: {
      type: DataTypes.STRING(50),
      field: "last_name",
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    tableName: "users",
    // bỏ qua createdAt, updataedAt
    timestamps: false,
  }
);

app.get("/api/v1/users", async (req, res) => {
  try {
    const users = await User.findAll();
    // Query DB thành công
    res.status(200).json({ data: users });
  } catch (error) {
    res.status(500).json({ data: error });
  }
});

app.listen(4000);
