class AppError extends Error {
  constructor(statusCode, message) {
    super(message);
    this.statusCode = statusCode;
  }
}

// err: instance của AppError
const handleErrors = (err, req, res, next) => {
  // Kiểm tra err của phải là lỗi của AppError hay không
  // Nếu err là instance của AppError, nghĩa là err là mình đã biết truoc1 và xử lý
  // Nếu là những lỗi k phải của AppError thì vì một lí do nào đó bị lỗi mà mình chưa biết
  if (!(err instanceof AppError)) {
    err = new AppError(500, "Internal Server");
  }
  const { message, statusCode } = err;
  res.status(statusCode).json({
    status: "error",
    message: message,
  });
  //    Nếu có các middleware phía sau, gọi next() để có thể đi tới các middleware phía sau
  next();
};

module.exports = {
  AppError,
  handleErrors,
};
