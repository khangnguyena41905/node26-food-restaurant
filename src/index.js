const express = require("express");
const { sequelize } = require("./models");
const { AppError, handleErrors } = require("./helpers/error");

const app = express();
app.use(express.json());

// Sync model của sequelize với DB
sequelize.sync({ alter: true });

const v1 = require("./routers/v1");
app.use("/api/v1", v1);

// demo handleErr
app.get("/error", (req, res, next) => {
  throw new AppError(500, "Internal Server");
});
// Middleware dùng để bắt và xử lý trả lỗi ra cho client
// Phải được đặt dưới các routers
app.use(handleErrors);

app.listen(3000);
