const { DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
  return sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      firstName: {
        type: DataTypes.STRING(50),
        field: "first_name",
      },
      lastName: {
        type: DataTypes.STRING(50),
        field: "last_name",
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: "invalid email",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        // validate:{
        //   // Demo custom validatiors
        //   isMatchedComfirmPassword:(value)=>{
        //     // logic validation
        //     // Nếu không thỏa mãn logic
        //     // throw new Error("message")
        //     if(value !== this.comfirmPassword){
        //       throw new Error("comfirm password not matchß")
        //     }
        //   }
        // }
        set(value) {
          // Không được lưu trực tiếp password xuống DB
          // Sử dụng bcrypt để hash password thành chuỗi ngẫu nhiên

          const salt = bcrypt.genSaltSync();
          const hashedPassword = bcrypt.hashSync(value, salt);
          this.setDataValue("password", hashedPassword);
        },
      },
    },
    {
      tableName: "users",
      // disable createdAt, updatedAt
      timestamps: false,
      defaultScope: {
        attributes: {
          exclude: ["password"],
        },
      },
      // các phương thức tự động chạy sau một hành động (create/update/delete)
      hooks: {
        //  Xóa password của record được trả ra sau khi tạo thành công trước khi trả ra cho client
        afterSave: (record) => {
          // dùng afterSave để khi create hoặc update thì đều xóa password của record trả về
          delete record.dataValues.password;
        },
      },
    }
  );
};
