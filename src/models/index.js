// Setup Sequelize
const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("node26-food", "root", "1234", {
  dialect: "mysql",
  host: "localhost",
  port: 3306,
});
// yarn add mysql2
(async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
})();

// khởi tạo model
const User = require("./User")(sequelize);
const Restaurant = require("./Restaurant")(sequelize);

// Định nghĩa relationship giữa các model
Restaurant.belongsTo(User, { foreignKey: "user_id" });
User.hasMany(Restaurant, { foreignKey: "user_id" });

module.exports = { sequelize, User, Restaurant };
