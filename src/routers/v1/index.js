// Router V1
const express = require("express");
const {
  getUsers,
  createUser,
  deleteUser,
  updateUser,
} = require("../../controllers/users.controller");

//  path v1: /api/v1
const v1 = express.Router();

// Định nghĩa các router cho users
v1.get("/users", getUsers());
v1.post("/users", createUser());
v1.delete("/users/:id", deleteUser());
v1.put("/users/:id", updateUser());

module.exports = v1;
