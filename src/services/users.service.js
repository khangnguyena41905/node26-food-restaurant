const { where } = require("sequelize");
const { AppError } = require("../helpers/error");
const { User, Restaurant } = require("../models");

// Service nhận vào data từ controller
//  Nhiệm vụ: xử lý nghiệp vụ của ứng dụng, sau đó gọi tới mdel của sequelize để query xuống DB, nhận data từ DB và return về cho controller

const getUsers = async () => {
  try {
    const users = await User.findAll({
      include: Restaurant,
    });
    return users;
  } catch (error) {
    throw new AppError(500, "Something went wrong with DB");
  }
};

const createUser = async (data) => {
  try {
    const user = await User.findOne({
      where: {
        email: data.email,
      },
    });
    // Email tồn tại trong DB
    if (user) {
      throw new AppError(400, "Email is existed");
    }

    //  Ví dụ trong trường hợp admin thêm user chỉ cần dùng email ta cần tạo ra password ngẫu nhiên
    if (!data.password) {
      data.password = Math.random().toString(36).substring(2);
      // Gửi email về cho user mật khẩu nàyk
    }

    const createdUser = await User.create(data);
    return createdUser;
  } catch (error) {
    throw error;
  }
};

const deleteUser = async (userID) => {
  try {
    const user = await User.findOne({
      where: {
        id: userID,
      },
    });

    if (!user) {
      throw new AppError(400, "User is not existed");
    }

    const deletedUser = await User.destroy({ where: { id: userID } });
    return deletedUser;
  } catch (error) {
    throw error;
  }
};

const updateUser = async (userID, data) => {
  try {
    const user = await User.findOne({
      where: {
        id: userID,
      },
    });

    if (!user) {
      throw new AppError(400, "User is not existed");
    }

    await User.update(data, { where: { id: userID } });
    const updatedUser = await User.findOne({
      where: {
        id: userID,
      },
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

// Delete:
// + User.findOne({where: {id: 1}}) - Nếu k tìm thấy thì trả về lỗi
// + User.destroy(where:{id:1})

// Update:
// + User.findOne({where: {id: 1}}) - Nếu k tìm thấy thì trả về lỗi
// + User.update(data, where:{id:1})
// + User.findOne({where:{id:1}})
module.exports = {
  getUsers,
  createUser,
  deleteUser,
  updateUser,
};
